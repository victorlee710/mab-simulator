import math

from strategy import Strategy


class UCB(Strategy):
    def __init__(self, arms, rounds):
        super().__init__(arms, rounds)

    def run(self):
        for t, sample in enumerate(self.samples):
            if t < len(self.arms):
                self.record(self.arms[t], self.arms[t].pull(sample))
                continue
            self.pick_arm(t, sample)

    def pick_arm(self, t, sample):
        best_arm = None
        high_ucb = None
        for i in self.arms:
            i_ucb = i.expected_mean() + (math.log(t - 1) / i.times) ** (1 / 2)
            if best_arm is None:
                best_arm = i
                high_ucb = i_ucb
            else:
                if i_ucb > high_ucb:
                    best_arm = i
                    high_ucb = i_ucb
        self.record(best_arm, best_arm.pull(sample))
