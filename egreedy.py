import random

from strategy import Strategy


class EGreedy(Strategy):
    def __init__(self, arms, rounds, e):
        super().__init__(arms, rounds)
        self.e = e

    def run(self):
        for s in self.samples:
            i = random.random()

            if i > self.e:
                self.exploit(s)
            else:
                self.explore(s)

    def exploit(self, sample):
        best_arm = self.get_expected_best_arm()
        self.record(best_arm, best_arm.pull(sample))

    def explore(self, sample):
        arm_choice = random.randint(0, len(self.arms) - 1)
        self.record(self.arms[arm_choice], self.arms[arm_choice].pull(sample))
