class Arm:
    def __init__(self, p):
        self.success = p
        self.times = 0
        self.reward = 0

    def __str__(self):
        return "Actual Mean: " + str(self.success) + "\n" \
               + "Expected Mean: " + str(self.expected_mean()) + "\n" \
               + "Times: " + str(self.times) + "\n" \
               + "Reward: " + str(self.reward)

    def pull(self, x):
        self.times = self.times + 1
        if x < self.success:
            self.reward = self.reward + 1
            return 1
        return 0

    def expected_mean(self):
        if self.times > 0:
            return self.reward / self.times
        else:
            return 0

    def reset(self):
        self.times = 0
        self.reward = 0
