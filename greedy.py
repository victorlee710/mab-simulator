import random

from strategy import Strategy


class Greedy(Strategy):
    def __init__(self, arms, rounds, sample_size):
        super().__init__(arms, rounds)
        self.sample_size = sample_size

    def run(self):
        best_arm = None
        for index, sample in enumerate(self.samples):
            if index < self.sample_size:
                arm_choice = random.randint(0, len(self.arms) - 1)
                self.record(self.arms[arm_choice], self.arms[arm_choice].pull(sample))
                continue

            if best_arm is None:
                best_arm = self.get_expected_best_arm()

            self.record(best_arm, best_arm.pull(sample))
