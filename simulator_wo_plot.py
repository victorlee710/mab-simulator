from arm import Arm
from egreedy import EGreedy
from greedy import Greedy
from ucb import UCB

ARMS_NUM = 4
SAMPLES = 50
ROUNDS = 1000
EPSILON = 0.1
M = 100

arms = [Arm(0.2), Arm(0.4), Arm(0.6), Arm(0.8)]
# Create arms by random
# for i in range(ARMS_NUM):
#     arm = Arm(random.random())
#     print("Arm " + str(i) + ": " + str(arm.success))
#     arms.append(arm)


# Create algos
greedy = Greedy(arms, ROUNDS, SAMPLES)
egreedy = EGreedy(arms, ROUNDS, EPSILON)
ucb = UCB(arms, ROUNDS)

# Run simulation
for m in range(M):
    seed = m
    greedy.start(seed)
    egreedy.start(seed)
    ucb.start(seed)

print("***** Greedy *****")
greedy.print_final_result()
print("***** E-Greedy *****")
egreedy.print_final_result()
print("***** UCB *****")
ucb.print_final_result()
