from matplotlib import pyplot

from arm import Arm
from egreedy import EGreedy
from greedy import Greedy
from ucb import UCB

ARMS_NUM = 4
SAMPLES = 50
ROUNDS = 1000
EPSILON = 0.1
M = 100

arms = [Arm(0.2), Arm(0.4), Arm(0.6), Arm(0.8)]
# Create arms by random
# for i in range(ARMS_NUM):
#     arm = Arm(random.random())
#     print("Arm " + str(i) + ": " + str(arm.success))
#     arms.append(arm)


# Create algos
greedy = Greedy(arms, ROUNDS, SAMPLES)
egreedy = EGreedy(arms, ROUNDS, EPSILON)
ucb = UCB(arms, ROUNDS)

# Run simulation
for m in range(M):
    seed = m
    greedy.start(seed)
    egreedy.start(seed)
    ucb.start(seed)

print("***** Greedy (Red) *****")
greedy.print_final_result()
print("***** E-Greedy (Green) *****")
egreedy.print_final_result()
print("***** UCB (Blue) *****")
ucb.print_final_result()

pyplot.figure(figsize=(14, 8))
pyplot.subplot('231')
pyplot.plot(greedy.get_final_average_cumulative_regret(), 'r')
pyplot.plot(egreedy.get_final_average_cumulative_regret(), 'g')
pyplot.plot(ucb.get_final_average_cumulative_regret(), 'b')
pyplot.ylabel('Regret')
pyplot.xlabel('Steps')

pyplot.subplot('232')
pyplot.plot(greedy.get_final_average_rewards(), 'r')
pyplot.plot(egreedy.get_final_average_rewards(), 'g')
pyplot.plot(ucb.get_final_average_rewards(), 'b')
pyplot.ylabel('Reward at time t')
pyplot.xlabel('Steps')

pyplot.subplot('233')
pyplot.plot(greedy.get_final_average_optimal_action(), 'r')
pyplot.plot(egreedy.get_final_average_optimal_action(), 'g')
pyplot.plot(ucb.get_final_average_optimal_action(), 'b')
pyplot.ylabel('% Optimal Action')
pyplot.xlabel('Steps')

bar_labels = ["Arm " + str(i + 1) for i in range(len(arms))]


def auto_label(rects):
    for rect in rects:
        height = rect.get_height()
        pyplot.text(rect.get_x() + rect.get_width() / 2., 1.05 * height,
                    '%d' % int(height),
                    ha='center', va='bottom')


pyplot.subplot('234')
auto_label(pyplot.bar(bar_labels, greedy.get_final_average_arms_times(), color='r'))
pyplot.xlabel("Average number of pulls for each arm in Greedy")
pyplot.subplot('235')
auto_label(pyplot.bar(bar_labels, egreedy.get_final_average_arms_times(), color='g'))
pyplot.xlabel("Average number of pulls for each arm in E-Greedy")
pyplot.subplot('236')
auto_label(pyplot.bar(bar_labels, ucb.get_final_average_arms_times(), color='b'))
pyplot.xlabel("Average number of pulls for each arm in UCB")

pyplot.tight_layout()
pyplot.show()
