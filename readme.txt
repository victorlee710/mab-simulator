Required plugins:
1. matplotlib (for simulator.py)

Configuration of the simulator:
# num of arms
	ARMS_NUM = 4
# number of samples for greedy algorithm
	SAMPLES = 50
# number of rounds per simulation
	ROUNDS = 1000
# epsilon for e-greedy algorithm
	EPSILON = 0.1
# number of simulation
	M = 100

Execution:
Run simulator_wo_plot.py for result without graph
Run simulator.py for more information
    1. Reward at each step
    2. Cumulative Regret at each step
    3. % Optimal Action at each step