import random


def add2list(list1, list2):
    return [x + y for x, y in zip(list1, list2)]


class Strategy:

    def __init__(self, arms, rounds):
        self.arms = arms
        self.rounds = rounds
        self.samples = []
        self.sim_times = 0
        self.history_actions = []
        self.history_rewards = []
        self.sum_of_avg_rewards = [0.0] * self.rounds
        self.sum_of_cum_regrets = [0.0] * self.rounds
        self.sum_of_optimal_actions = [0.0] * self.rounds
        self.sum_of_arms_sel_times = [0] * len(self.arms)
        self.sum_of_avg_reward_ind_arms = [0.0] * len(self.arms)
        self.best_arm = self.get_best_arm()

    def setup(self, seed):
        random.seed(seed)
        self.samples = [random.random() for _ in range(self.rounds)]

    def start(self, seed):
        self.sim_times = self.sim_times + 1
        self.setup(seed)
        self.run()
        self.get_result()
        self.reset()

    def reset(self):
        [i.reset() for i in self.arms]
        self.history_actions = []
        self.history_rewards = []

    def run(self):
        pass

    def record(self, i, r):
        self.history_actions.append(i)
        # For average single reward
        self.history_rewards.append(r)

    # Add the result for each simulation
    def get_result(self):
        self.sum_of_cum_regrets = add2list(self.sum_of_cum_regrets, self.get_regret())
        self.sum_of_avg_rewards = add2list(self.sum_of_avg_rewards, self.history_rewards)
        self.sum_of_optimal_actions = add2list(self.sum_of_optimal_actions, self.get_optimal_actions())
        self.sum_of_arms_sel_times = add2list(self.sum_of_arms_sel_times, [i.times for i in self.arms])

        print(self.__class__.__name__ + " Simulation " + str(self.sim_times))
        for i in range(len(self.arms)):
            arm = self.arms[i]
            print("Arm " + str(i + 1) + " - Times: " + str(arm.times) + "; Average reward: " + str(arm.expected_mean()))
            self.sum_of_avg_reward_ind_arms[i] = self.sum_of_avg_reward_ind_arms[i] + arm.expected_mean()

    def get_regret(self):
        best_mean = self.best_arm.success
        regret = []
        for i in self.history_actions:
            if len(regret) == 0:
                regret.append(best_mean - i.success)
            else:
                regret.append(regret[-1] + best_mean - i.success)
        return regret

    def get_optimal_actions(self):
        fractions = [x + 1 for x in range(self.rounds)]
        best_arm = self.best_arm
        optimal_actions = []
        for i in self.history_actions:
            result = 0
            if i == best_arm:
                result = 1
            if len(optimal_actions) > 0:
                optimal_actions.append(optimal_actions[-1] + result)
            else:
                optimal_actions.append(result)
        result = [a / b * 100 for a, b in zip(optimal_actions, fractions)]
        return result

    # Find the best arm at time t
    def get_best_arm(self):
        best_arm = None
        high_mean = None

        for i in self.arms:
            mean = i.success
            if best_arm is not None:
                if high_mean < mean:
                    high_mean = mean
                    best_arm = i
            else:
                high_mean = mean
                best_arm = i
        return best_arm

    def get_expected_best_arm(self):
        best_arm = None
        high_mean = None

        for i in self.arms:
            mean = i.expected_mean()
            if best_arm is not None:
                if high_mean < mean:
                    high_mean = mean
                    best_arm = i
            else:
                high_mean = mean
                best_arm = i
        return best_arm

    def print_final_result(self):
        print("Average Reward of the algorithm: " + str(self.sum_of_avg_rewards[-1] / self.sim_times))
        print("Average Regret of the algorithm: " + str(self.sum_of_cum_regrets[-1] / self.sim_times))
        print("% Optimal Action: " + str(self.sum_of_optimal_actions[-1] / self.sim_times))
        for i in range(len(self.sum_of_avg_reward_ind_arms)):
            print(
                "Average Reward of Arm " + str(i + 1) + ": " + str(self.sum_of_avg_reward_ind_arms[i] / self.sim_times))
        for i in range(len(self.sum_of_arms_sel_times)):
            print("Average pull of Arm " + str(i + 1) + ": " + str(self.sum_of_arms_sel_times[i] / self.sim_times))


    def get_final_average_rewards(self):
        return [x / self.sim_times for x in self.sum_of_avg_rewards]

    def get_final_average_cumulative_regret(self):
        return [x / self.sim_times for x in self.sum_of_cum_regrets]

    def get_final_average_optimal_action(self):
        return [x / self.sim_times for x in self.sum_of_optimal_actions]

    def get_final_average_arms_times(self):
        return [x / self.sim_times for x in self.sum_of_arms_sel_times]
